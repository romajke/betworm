﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Betworm.Domain
{
    public interface ICatalogRepository
    {
        Task<List<Event>> GetEventListAsync(string token, int parentEventId);

        Task<List<Market>> GetMarketListAsync(string token, int eventId);

        Task<List<Runner>> GetRunnerListAsync(string token, int marketId);
    }
}
