﻿
namespace Betworm.Domain
{
    public class PlacedBet
    {
        private readonly long _betId;
        private readonly decimal _averagePriceMatched;
        private readonly decimal _sizeMatched;
        private readonly bool _success;
        private readonly string _resultCode;

        public long BetId { get { return _betId; } }
        public decimal AveragePriceMatched { get { return _averagePriceMatched; } }
        public decimal SizeMatched { get { return _sizeMatched; } }
        public bool Success { get { return _success; } }
        public string ResultCode { get { return _resultCode; } }

        public PlacedBet(long betId, decimal averagePriceMatched, decimal sizeMatched, bool success, string resultCode)
        {
            _betId = betId;
            _averagePriceMatched = averagePriceMatched;
            _sizeMatched = sizeMatched;
            _success = success;
            _resultCode = resultCode;
        }

        public override string ToString()
        {
            return string.Format("BetId: {0}; AveragePriceMatched: {1}; SizeMatched: {2}; Success: {3}; ResultCode: {4}", _betId, _averagePriceMatched, _sizeMatched, _success, _resultCode);
        }
    }
}
