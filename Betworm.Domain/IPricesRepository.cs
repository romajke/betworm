﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Betworm.Domain
{
    public interface IPricesRepository
    {
        Task<Market.PricesResult> GetMarketPriceListAsync(string token, int marketId);
    }
}
