﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betworm.Domain
{
    public enum BetType { Back, Lay }

    public class Bet
    {
        private readonly int _marketId;
        private readonly int _runnerId;
        private readonly BetType _type;
        private readonly decimal _price;
        private readonly decimal _size;
        private readonly bool _cancelUnmatchedInPlay;

        public int MarketId { get { return _marketId; } }
        public int RunnerId { get { return _runnerId; } }
        public BetType Type { get { return _type; } }
        public decimal Price { get { return _price; } }
        public decimal Size { get { return _size; } }
        public bool CancelUnmatchedInPlay { get { return _cancelUnmatchedInPlay; } }

        public Bet(int marketId, int runnerId, BetType type, decimal price, decimal size, bool cancelUnmatchedInPlay)
        {
            _marketId = marketId;
            _runnerId = runnerId;
            _type = type;
            _price = price;
            _size = size;
            _cancelUnmatchedInPlay = cancelUnmatchedInPlay;
        }

        public async Task<PlacedBet> Place(string token, IBetsRepository bettingRepository)
        {
            if (bettingRepository == null)
                throw new ArgumentNullException("bettingRepository");

            List<Bet> betList = new List<Bet> { this };

            List<PlacedBet> betPlacementResultList = await bettingRepository
                .PlaceBetsAsync(token, betList);

            PlacedBet betPlacementResult = betPlacementResultList.Single();

            return betPlacementResult;
        }

        public override string ToString()
        {
            return string.Format("MarketId: {0}; RunnerId: {1}; Type: {2}; Price: {3}; Size: {4}; CancelUnmatchedInPlay: {5}; ", _marketId, _runnerId, _type, _price, _size, _cancelUnmatchedInPlay);
        }
    }
}
