﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Betworm.Domain
{
    public class Distance : IComparable<Distance>
    {
        private readonly int _miles;
        private readonly int _furlongs;

        public Distance(int miles, int furlongs)
        {
            if (miles < 0)
                throw new ArgumentException("value must be non negative", "miles");

            if (furlongs < 0)
				throw new ArgumentException("value must be nin negative", "furlongs");

            _miles = miles;
            _furlongs = furlongs;
        }

        private int TotalFeets()
        {
			int feets = _miles * 5280 + _furlongs * 660;
			return feets;
        }

        public int CompareTo(Distance other)
        {
            if (other == null)
                throw new ArgumentNullException("other");

            int result = 
				this.TotalFeets()
					.CompareTo(
						other.TotalFeets());

            return result;
        }

        public override string ToString()
        {
            return string.Format("{0}m{1}f", _miles, _furlongs);
        }

        private static readonly Dictionary<string, Distance> _cache = new Dictionary<string, Distance>();

        public static Distance Parse(string value, bool caching = true)
        {
            Distance distance;

			string pattern = @"((?<miles>[1-8])m(?<furlongs>[1-8])f)|((?<miles>[1-8])m)|((?<furlongs>[1-8])f)";

			Regex regex = new Regex(pattern);

			Match match = regex.Match(value);

			if (!match.Success)
				return null;

			int miles = 0;
			int furlongs = 0;

			Group milesGroup = match.Groups["miles"];
			Group furlongsGroup = match.Groups["furlongs"];

			if (milesGroup.Success)
				miles = Convert.ToInt32(milesGroup.Value);

			if (furlongsGroup.Success)
				furlongs = Convert.ToInt32(furlongsGroup.Value);

			string key = string.Format("{0}_{1}", miles, furlongs);

			if (!caching || !_cache.TryGetValue(key, out distance))
			{
				distance = new Distance(miles, furlongs);

				_cache.Add(key, distance);
			}

            return distance;
        }
    }
}
