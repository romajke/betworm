﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Betworm.Domain
{
    public class Event
    {
        private readonly int _id;
        private readonly string _name;

        public int Id { get { return _id; } }
        public string Name { get { return _name; } }

        public Event(int id, string name)
        {
            _id = id;
            _name = name;
        }

        public async Task<List<Market>> GetMarketListAsync(string token, ICatalogRepository catalogRepository)
        {
            if (catalogRepository == null)
                throw new ArgumentNullException("catalogRepository");

            List<Market> marketList = await catalogRepository
                .GetMarketListAsync(token: token, eventId: _id);

            return marketList;
        }

        public override string ToString()
        {
            return string.Format("Id: {0}; Name: {1};", _id, _name);
        }
    }
}
