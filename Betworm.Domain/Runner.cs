﻿
namespace Betworm.Domain
{
    public class Runner
    {
        private readonly int _id;
        private readonly string _name;

        public int Id { get { return _id; } }
        public string Name { get { return _name; } }

        public Runner(int id, string name)
        {
            _id = id;
            _name = name;
        }

        public override string ToString()
        {
            return string.Format("Id: {0}; Name: {1};", _id, _name);
        }
    }
}
