﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Betworm.Domain
{
    public class Price
    {
        private readonly int _runnerId;
        private readonly IEnumerable<decimal> _backPriceList;
        private readonly IEnumerable<decimal> _layPriceList;

        public int RunnerId { get { return _runnerId; } }
        public IEnumerable<decimal> BackPriceList { get { return _backPriceList; } }
        public IEnumerable<decimal> LayPriceList { get { return _layPriceList; } }

        public Price(int runnerId, IEnumerable<decimal> backPriceList, IEnumerable<decimal> layPriceList)
        {
            if (backPriceList == null)
                throw new ArgumentNullException("backPriceList");

            if (layPriceList == null)
                throw new ArgumentNullException("layPriceList");

            _runnerId = runnerId;
            _backPriceList = backPriceList;
            _layPriceList = layPriceList;
        }

        public decimal? GetBestLayPrice()
        {
			decimal? price = _layPriceList.Any()
				? _layPriceList.Min()
				: (decimal?)null;

            return price;
        }

        public decimal? GetBestBackPrice()
        {
			decimal? price = _backPriceList.Any()
				? _backPriceList.Min()
				: (decimal?)null;

            return price;
        }

        public override string ToString()
        {
            return string.Format("RunnerId: {0}; BackPriceList: [{1}]; LayPriceList: [{2}]", _runnerId, string.Join("|", _backPriceList), string.Join("|", _layPriceList));
        }
    }
}
