﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Betworm.Domain
{
    public interface IBetsRepository
    {
        Task<List<PlacedBet>> PlaceBetsAsync(string token, List<Bet> bets);
    }
}
