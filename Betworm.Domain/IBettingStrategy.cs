﻿using System.Collections.Generic;

namespace Betworm.Domain
{
    public interface IBettingStrategy
    {
		int GetEventId();

        bool IsApproved(Market market);

		List<Bet> OfferBets(Market market, List<Price> prices, List<PlacedBet> placedBets);
    }
}
