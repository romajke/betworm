﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Betworm.Domain
{
    public class Market
    {
        public class PricesResult
        {
            public bool IsMarketInPlay { get; set; }
            public List<Price> PriceList { get; set; }
        }

        private readonly int _id;
        private readonly DateTime _startTime;
        private readonly string _name;
        private readonly int _numberOfWinners;
        private readonly string _venue;

        public int Id { get { return _id; } }
        public DateTime StartTime { get { return _startTime; } }
        public string Name { get { return _name; } }
        public int NumberOfWinners { get { return _numberOfWinners; } }
        public string Venue { get { return _venue; } }

        private bool? _isMarketInPlay;
        public bool? IsMarketInPlay { get { return _isMarketInPlay; } }

        public Market(int id, DateTime startTime, string name, int numberOfWinners, string venue)
        {
            _id = id;
            _startTime = startTime;
            _name = name;
            _numberOfWinners = numberOfWinners;
            _venue = venue;
        }

        public async Task<List<Runner>> GetRunnerListAsync(string token, ICatalogRepository catalogRepository)
        {
            if (catalogRepository == null)
                throw new ArgumentNullException("catalogRepository");

            List<Runner> runnerList = await catalogRepository
                .GetRunnerListAsync(token, marketId: _id);

            return runnerList;
        }

        public async Task<List<Price>> GetPriceListAsync(string token, IPricesRepository pricesRepository)
        {
            if (pricesRepository == null)
                throw new ArgumentNullException("pricesRepository");

            PricesResult result = await pricesRepository
                .GetMarketPriceListAsync(token, marketId: _id);

            _isMarketInPlay = result.IsMarketInPlay;

            return result.PriceList;
        }

        public async Task<List<PlacedBet>> PlaceBetsAsync(string token, IBetsRepository bettingRepository, params Bet[] bets)
        {
            if (bettingRepository == null)
                throw new ArgumentNullException("bettingRepository");

            return await PlaceBetsAsync(token, bettingRepository, bets.ToList());
        }

        public async Task<List<PlacedBet>> PlaceBetsAsync(string token, IBetsRepository bettingRepository, IEnumerable<Bet> bets)
        {
            if (bettingRepository == null)
                throw new ArgumentNullException("bettingRepository");

            return await PlaceBetsAsync(token, bettingRepository, bets.ToList());
        }

        public async Task<List<PlacedBet>> PlaceBetsAsync(string token, IBetsRepository bettingRepository, List<Bet> bets)
        {
            if (bettingRepository == null)
                throw new ArgumentNullException("bettingRepository");

            List<PlacedBet> placingBetResultList = await bettingRepository
                .PlaceBetsAsync(token, bets);

            return placingBetResultList;
        }

        public override string ToString()
        {
            return string.Format("Id: {0}; StartTime: {1}; Name: {2}; NumberOfWinners: {3}; Venue: {4};", _id, _startTime, _name, _numberOfWinners, _venue);
        }
    }
}
