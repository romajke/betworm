﻿using System.Threading.Tasks;

namespace Betworm.Domain
{
    public interface ISessionRepository
    {
        Task<string> GetSessionTokenAsync(string userName, string password);

        Task<string> ProlongSessionAsync(string token);
    }
}
