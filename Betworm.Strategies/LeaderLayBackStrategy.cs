﻿using Betworm.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Betworm.Strategies
{
    public class LeaderLayBackStrategy : IBettingStrategy
    {
        private readonly Distance _maxDistance;

        private readonly decimal _minLayPrice;
        private readonly decimal _maxLayPrice;

        private readonly decimal _laySize;
        private readonly decimal _backSize;

        public LeaderLayBackStrategy(
            string maxDistance = "1m2f",
            decimal minLayPrice = 2m,
            decimal maxLayPrice = 3m,
            decimal laySize = 6m,
            decimal backSize = 5m)
        {
            _maxDistance = Distance.Parse(maxDistance);

            _minLayPrice = minLayPrice;
            _maxLayPrice = maxLayPrice;

            _laySize = laySize;
            _backSize = backSize;
        }

		public int GetEventId()
		{
			// today GB races
			return 298251;
		}

        public bool IsApproved(Market market)
        {
            if (market == null)
                throw new ArgumentNullException("market");

            bool isSingleWinner = market.NumberOfWinners == 1;
			if (!isSingleWinner)
				return false;

			DateTime utcNow = DateTime.UtcNow;

			bool isToday = market.StartTime.Date == utcNow.Date;
			if (!isToday)
				return false;

			bool isStartedMoreThan10MinutesAgo = market.StartTime.AddMinutes(10) < utcNow;
			if (isStartedMoreThan10MinutesAgo)
				return false;

			Distance distance = Distance.Parse(market.Name);
			if (distance == null)
				return false;

			bool isShortDistance = distance.CompareTo(_maxDistance) <= 0;   // distance less or equal max distance

			return isShortDistance;
        }

		public List<Bet> OfferBets(Market market, List<Price> prices, List<PlacedBet> placedBets)
        {
			if (market == null)
				throw new ArgumentNullException("market");

            if (prices == null)
                throw new ArgumentNullException("prices");

            if (placedBets == null)
                throw new ArgumentNullException("placedBets");

            List<Bet> betList = new List<Bet>();

			bool isMarketInPlay = market.IsMarketInPlay.HasValue && market.IsMarketInPlay.Value;
			bool isPlacedBetsExists = placedBets != null && placedBets.Any();

			// не предлагаем ставки в In Play
			if (isMarketInPlay)
				return betList;

			// не предлагаем, если уже есть ставки
            if (isPlacedBetsExists)
                return betList;

            foreach (Price price in prices)
            {
                decimal? bestLayPrice = price.GetBestLayPrice();

                // QUESTION: как быть с рынками, где есть бегун с кэфом от 2 до 3, но есть и бегуны с меньшим кэфом (до 2) - то есть он нам подходит, но не является лидером

                if (bestLayPrice.HasValue && 
					bestLayPrice >= _minLayPrice && 
					bestLayPrice <= _maxLayPrice)
                {
                    Bet layBet = new Bet(
						marketId: market.Id,
                        runnerId: price.RunnerId,
                        type: BetType.Lay,
                        price: bestLayPrice.Value,
                        size: _laySize,
                        cancelUnmatchedInPlay: false);

                    betList.Add(layBet);

                    decimal profit = _laySize - _backSize;
                    decimal layBetLose = -(bestLayPrice.Value - 1) * _laySize;
                    decimal backBetWin = -layBetLose + profit;
                    decimal backPrice = (backBetWin / _backSize) + 1;

                    Bet backBet = new Bet(
                        marketId: market.Id,
                        runnerId: price.RunnerId,
                        type: BetType.Back,
                        price: backPrice,
                        size: _backSize,
                        cancelUnmatchedInPlay: false);

                    betList.Add(backBet);
                }
            }

            return betList;
        }
	}
}
