﻿using Betworm.Data.Remote.BetfairGlobal;
using Betworm.Domain;
using System.Threading.Tasks;

namespace Betworm.Data.Remote
{
    public class RemoteSessionRepository : RemoteRepositoryBase, ISessionRepository
    {
        public async Task<string> GetSessionTokenAsync(string userName, string password)
        {
            BFGlobalServiceClient betfairGlobal = new BFGlobalServiceClient();

            LoginReq request = new LoginReq
            {
                ipAddress = "0",
                locationId = 0,
                password = password,
                productId = 82,
                username = userName,
                vendorSoftwareId = 0
            };

            loginOut response = await betfairGlobal.loginAsync(request);

			ValidateErrorCode(response.Result.errorCode.ToString(), response.Result.minorErrorCode);
			ValidateErrorCode(response.Result.header.errorCode.ToString(), response.Result.header.minorErrorCode);

            string sessionToken = response.Result.header.sessionToken;

            return sessionToken;
        }


        public async Task<string> ProlongSessionAsync(string token)
        {
            BFGlobalServiceClient betfairGlobal = new BFGlobalServiceClient();

            KeepAliveReq request = new KeepAliveReq
            {
                header = new APIRequestHeader
                {
                    clientStamp = 0,
                    sessionToken = token
                }
            };

            keepAliveOut response = await betfairGlobal.keepAliveAsync(request);

			ValidateErrorCode(response.Result.header.errorCode.ToString(), response.Result.header.minorErrorCode);

            string sessionToken = response.Result.header.sessionToken;

            return sessionToken;
        }
    }
}
