﻿using Betworm.Data.Remote.BetfairExchange;
using Betworm.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betworm.Data.Remote
{
    public class RemoteBetsRepository : RemoteRepositoryBase, IBetsRepository
    {
        public async Task<List<Domain.PlacedBet>> PlaceBetsAsync(string token, List<Domain.Bet> bets)
        {
            if (bets == null)
                throw new ArgumentNullException("bets");

            BFExchangeServiceClient betfairExchange = new BFExchangeServiceClient();

            Func<BetType, BetTypeEnum> convertBetType = betTypeEnum =>
            {
                switch (betTypeEnum)
                {
                    case BetType.Back: return BetTypeEnum.B;
                    case BetType.Lay: return BetTypeEnum.L;
                    default: throw new Exception("invalid bet type");
                }
            };

            PlaceBets[] placeBets = bets
                .Select(b => new PlaceBets
                {
                    betCategoryType = BetCategoryTypeEnum.E,
                    betPersistenceType = b.CancelUnmatchedInPlay ? BetPersistenceTypeEnum.NONE : BetPersistenceTypeEnum.IP,
                    betType = convertBetType(b.Type),
                    marketId = b.MarketId,
                    price = Convert.ToDouble(b.Price),
                    selectionId = b.RunnerId,
                    size = Convert.ToDouble(b.Size)
                })
                .ToArray();

            PlaceBetsReq request = new PlaceBetsReq
            {
                bets = placeBets,
                header = new APIRequestHeader
                {
                    clientStamp = 0,
                    sessionToken = token
                }
            };

            placeBetsOut response = await betfairExchange.placeBetsAsync(request);

			ValidateErrorCode(response.Result.errorCode.ToString(), response.Result.minorErrorCode);

            List<Domain.PlacedBet> placingBetResultList = response
                .Result
                .betResults
                .Select(br => new Domain.PlacedBet(
                    betId: br.betId,
                    averagePriceMatched: Convert.ToDecimal(br.averagePriceMatched),
                    sizeMatched: Convert.ToDecimal(br.sizeMatched),
                    success: br.success,
                    resultCode: br.resultCode.ToString()))
                .ToList();

            return placingBetResultList;
        }


    }
}
