﻿using Betworm.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GlobalAPIRequestHeader = Betworm.Data.Remote.BetfairGlobal.APIRequestHeader;
using ExchangeAPIRequestHeader = Betworm.Data.Remote.BetfairExchange.APIRequestHeader;
using Betworm.Data.Remote.BetfairGlobal;
using Betworm.Data.Remote.BetfairExchange;

namespace Betworm.Data.Remote
{
    public class RemoteCatalogRepository : RemoteRepositoryBase, ICatalogRepository
    {
        public async Task<List<Event>> GetEventListAsync(string token, int parentEventId)
        {
            BFGlobalServiceClient betfairGlobal = new BFGlobalServiceClient();

            GetEventsReq request = new GetEventsReq
            {
                eventParentId = parentEventId,
                locale = "en",
                header = new GlobalAPIRequestHeader
                {
                    clientStamp = 0,
                    sessionToken = token
                }
            };

            getEventsOut response = await betfairGlobal.getEventsAsync(request);

			ValidateErrorCode(response.Result.errorCode.ToString(), response.Result.minorErrorCode);

            List<Event> eventList = response
                .Result
                .eventItems
                .Select(ei => new Event(
                    id: ei.eventId,
                    name: ei.eventName))
                .ToList();

            return eventList;
        }

        public async Task<List<Domain.Market>> GetMarketListAsync(string token, int eventId)
        {
            BFGlobalServiceClient betfairGlobal = new BFGlobalServiceClient();

            GetEventsReq request = new GetEventsReq
            {
                eventParentId = eventId,
                locale = "en",
                header = new GlobalAPIRequestHeader
                {
                    clientStamp = 0,
                    sessionToken = token
                }
            };

            getEventsOut response = await betfairGlobal.getEventsAsync(request);

			ValidateErrorCode(response.Result.errorCode.ToString(), response.Result.minorErrorCode);

            List<Domain.Market> marketList = response
                .Result
                .marketItems
                .Select(mi => new Domain.Market(
                    id: mi.marketId,
                    startTime: mi.startTime,
                    name: mi.marketName,
                    numberOfWinners: mi.numberOfWinners,
                    venue: mi.venue))
                .ToList();

            return marketList;
        }


        public async Task<List<Domain.Runner>> GetRunnerListAsync(string token, int marketId)
        {
            BFExchangeServiceClient betfairExchange = new BFExchangeServiceClient();

            GetMarketReq request = new GetMarketReq
            {
                includeCouponLinks = false,
                locale = "en",
                marketId = marketId,
                header = new ExchangeAPIRequestHeader
                {
                     clientStamp = 0,
                     sessionToken = token
                }
            };

            getMarketOut response = await betfairExchange.getMarketAsync(request);

			ValidateErrorCode(response.Result.errorCode.ToString(), response.Result.minorErrorCode);

            List<Domain.Runner> runnerList = response
                .Result
                .market
                .runners
                .Select(r => new Domain.Runner(
                    id: r.selectionId,
                    name: r.name))
                .ToList();

            return runnerList;
        }
    }
}
