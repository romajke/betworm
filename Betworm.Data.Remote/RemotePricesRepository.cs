﻿using Betworm.Data.Remote.BetfairExchange;
using Betworm.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betworm.Data.Remote
{
    public class RemotePricesRepository : RemoteRepositoryBase, IPricesRepository
    {
        public async Task<Domain.Market.PricesResult> GetMarketPriceListAsync(string token, int marketId)
        {
            BFExchangeServiceClient betfairExchange = new BFExchangeServiceClient();

            GetMarketPricesReq request = new GetMarketPricesReq
            {
                currencyCode = "USD",
                marketId = marketId,
                header = new APIRequestHeader
                {
                    clientStamp = 0,
                    sessionToken = token
                }
            };

            getMarketPricesOut response = await betfairExchange.getMarketPricesAsync(request);

            ValidateErrorCode(response.Result.errorCode.ToString(), response.Result.minorErrorCode);

            List<Domain.Price> priceList = response
                .Result
                .marketPrices
                .runnerPrices
                .Select(p => new Domain.Price(
                    runnerId: p.selectionId,
                    backPriceList: p.bestPricesToBack.Select(bp => Convert.ToDecimal(bp.price)).ToList(),
                    layPriceList: p.bestPricesToLay.Select(bp => Convert.ToDecimal(bp.price)).ToList()))
                .ToList();

            bool isMarketInPlay = response.Result.marketPrices.delay > 0;

            Domain.Market.PricesResult result = new Domain.Market.PricesResult
            {
                IsMarketInPlay = isMarketInPlay,
                PriceList = priceList
            };

            return result;
        }
    }
}
