﻿using System;

namespace Betworm.Data.Remote
{
    public abstract class RemoteRepositoryBase
    {
        public void ValidateErrorCode(string errorCode, string minorErrorCode)
        {
            if (errorCode != "OK")
				 throw new Exception(string.Format("error (code = {0}, minorErrorCode={1}) in service response", errorCode, minorErrorCode));
        }
    }
}
