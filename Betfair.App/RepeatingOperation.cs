﻿using System;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace Betfair.App
{
    public class RepeatingOperation
    {
        private readonly TimeSpan _interval;

        private readonly Action _action;

        private DispatcherTimer _timer;

        public RepeatingOperation(TimeSpan? interval, Action action)
        {
            if (action == null)
                throw new ArgumentNullException("action");

            _interval = interval ?? TimeSpan.FromSeconds(1);
            _action = action;

            _timer = new DispatcherTimer(DispatcherPriority.Normal);
            _timer.Interval = _interval;

            _timer.Tick += (s, e) => _action();
        }

        public void Perform(bool deferred = true)
        {
            if (!deferred)
                _action();

            _timer.Start();
        }

        public void Stop()
        {
            if (_timer != null)
                _timer.Stop();
        }
    }
}
