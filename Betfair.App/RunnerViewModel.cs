﻿using Betworm.Domain;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Betfair.App
{
    public class RunnerViewModel : ViewModelBase
    {
        public MarketViewModel MarketViewModel { get; set; }

        private Runner _runner;
        public Runner Runner
        {
            get { return _runner; }
            set
            {
                _runner = value;
                RaisePropertyChanged("Runner");
            }
        }

        private Price _price;
        public Price Price
        {
            get { return _price; }
            set
            {
                _price = value;
                RaisePropertyChanged("Price");
            }
        }

        private decimal? _bestBackPrice;
        public decimal? BestBackPrice
        {
            get { return _bestBackPrice; }
            set
            {
                _bestBackPrice = value;
                RaisePropertyChanged("BestBackPrice");
            }
        }

        private decimal? _bestLayPrice;
        public decimal? BestLayPrice
        {
            get { return _bestLayPrice; }
            set
            {
                _bestLayPrice = value;
                RaisePropertyChanged("BestLayPrice");
            }
        }

        private ObservableCollection<Bet> _offeredBetList;
        public ObservableCollection<Bet> OfferedBetList
        {
            get { return _offeredBetList; }
            set
            {
                _offeredBetList = value;
                RaisePropertyChanged("OfferedBetList");
            }
        }

        private int _betCount;
        public int BetCount
        {
            get { return _betCount; }
            set
            {
                _betCount = value;
                RaisePropertyChanged("BetCount");
            }
        }

        // SPECIALIZED: properties special for LeaderLayBackStrategy

        private Bet _layBet;
        public Bet LayBet
        {
            get { return _layBet; }
            set
            {
                _layBet = value;
                RaisePropertyChanged("LayBet");
            }
        }

        private Bet _backBet;
        public Bet BackBet
        {
            get { return _backBet; }
            set
            {
                _backBet = value;
                RaisePropertyChanged("BackBet");
            }
        }
    }
}
