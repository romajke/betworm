﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace System.Collections.Generic
{
    public static class EnumerableExtensions
    {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
                throw new ArgumentNullException("enumerable");

            ObservableCollection<T> result = new ObservableCollection<T>();

            lock (enumerable)
            {
                foreach (T item in enumerable)
                {
                    result.Add(item);
                }
            }

            return result;
        }

        public static IEnumerable<T> ForEachReturn<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            if (enumerable == null)
                throw new ArgumentNullException("enumerable");

            foreach (T item in enumerable)
            {
                action(item);
            }

            return enumerable;
        }

        public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> range)
        {
            if (collection == null)
                throw new ArgumentNullException("collection");

            if (range == null)
                throw new ArgumentNullException("range");

            foreach (T item in range)
            {
                collection.Add(item);
            }
        }
    }
}
