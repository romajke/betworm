﻿using Betworm.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Betfair.App
{
    public class Controller
    {
        private static readonly Lazy<Controller> lazy = new Lazy<Controller>(() => new Controller());
        public static Controller Instance { get { return lazy.Value; } }
        public Controller()
        {
            _mainViewModel = new MainViewModel();
            _mainViewModel.PropertyChanged += OnMainViewModelPropertyChanged;

            _repositories = new Repositories(ConfigurationManager.AppSettings["repo_code"]);
            _strategies = new Strategies();
        }

        private async void OnMainViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "SelectedMarket":
                    if (_mainViewModel.SelectedMarket != null)
                    {
                        await LoadSelectedMarketRunners();
                    }
                    break;
                case "MarketList":
                    if (_mainViewModel.MarketList != null && _mainViewModel.MarketList.Any())
                    {
                        _mainViewModel.SelectedMarket = _mainViewModel.MarketList.First();
                    }
                    break;
            }
        }

        private void OnMarketViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "RunnerList":
                    if (_mainViewModel.SelectedMarket != null && _mainViewModel.SelectedMarket.RunnerList != null)
                    {
                        if (_loadMarketPricesOperation != null)
                            _loadMarketPricesOperation.Stop();

						int refreshPricesTimeoutSec = Convert.ToInt32(ConfigurationManager.AppSettings["refresh_prices_sec"]);

						_loadMarketPricesOperation = new RepeatingOperation(TimeSpan.FromSeconds(refreshPricesTimeoutSec), async () =>
                        {
                            if (_mainViewModel.SelectedMarket != null && _mainViewModel.SelectedMarket.RunnerList != null)
                            {
                                await LoadSelectedMarketPrices();
                            }

							// всё могло измениться ;)
							if (_mainViewModel.SelectedMarket != null && _mainViewModel.SelectedMarket.RunnerList != null)
							{
                                OfferBets();
							}
                        });

                        _loadMarketPricesOperation.Perform(deferred: false);
                    }
                    break;
            }
        }

        private Strategies _strategies;
        private Repositories _repositories;
        private RepeatingOperation _keepSessionOperation;
        private RepeatingOperation _loadMarketPricesOperation;

        private string _token;

        private readonly MainViewModel _mainViewModel;
        public MainViewModel MainViewModel { get { return _mainViewModel; } }

        private readonly Dictionary<int, RunnerViewModel> _runnerViewModelDictionary = new Dictionary<int, RunnerViewModel>();  // fill on load runners, use on load prices for mixing prices with runners

        public async Task Login(bool keepAlive)
        {
            _mainViewModel.IsLoggedIn = false;

            string userName = ConfigurationManager.AppSettings["user_name"];
            string password = ConfigurationManager.AppSettings["password"];

            ISessionRepository sessionRepository = _repositories.SessionRepository;

            _token = await sessionRepository
                .GetSessionTokenAsync(userName, password);

            _mainViewModel.IsLoggedIn = true;

            if (keepAlive)
            {
                int keepAliveTimeoutMin = Convert.ToInt32(ConfigurationManager.AppSettings["keep_alive_timeout_min"]);

                if (_keepSessionOperation != null)
                    _keepSessionOperation.Stop();
                _keepSessionOperation = new RepeatingOperation(TimeSpan.FromMinutes(keepAliveTimeoutMin), async () =>
                {
                    _token = await sessionRepository
                        .ProlongSessionAsync(_token);
                });

                _keepSessionOperation.Perform(deferred: true);
            }
        }

        public async Task LoadMarkets()
        {
            if (_mainViewModel.IsMarketsLoading)
                return;

            _mainViewModel.IsMarketsLoading = true;

            if (_mainViewModel.MarketList != null)
            {
                _mainViewModel.MarketList.ForEachReturn(mvm => mvm.PropertyChanged -= OnMarketViewModelPropertyChanged);
                _mainViewModel.MarketList = null;
            }

            _mainViewModel.SelectedMarket = null;
            if (_loadMarketPricesOperation != null)
                _loadMarketPricesOperation.Stop();

            IBettingStrategy bettingStrategy = _strategies.CurrentBettingStrategy;

            ICatalogRepository catalogRepository = _repositories.CatalogRepository;

			int todayGBRacesEventId = bettingStrategy.GetEventId();

            List<Event> eventList = await catalogRepository
                .GetEventListAsync(_token, parentEventId: todayGBRacesEventId);

            List<MarketViewModel> marketViewModelList = new List<MarketViewModel>();

            foreach (Event @event in eventList)
            {
                List<Market> marketList = await @event
                    .GetMarketListAsync(_token, catalogRepository);

                foreach (Market market in marketList)
                {
                    MarketViewModel marketViewModel = new MarketViewModel
                    {
                        Event = @event,
                        Market = market
                    };

                    marketViewModel.PropertyChanged += OnMarketViewModelPropertyChanged;

					bool isMarketApproved = bettingStrategy.IsApproved(marketViewModel.Market);

					if (isMarketApproved)
					{
						marketViewModelList.Add(marketViewModel);
					}
                }
            }

            _mainViewModel.MarketList = marketViewModelList
                .OrderBy(mvm => mvm.Market.StartTime)
                .ToObservableCollection();

            _mainViewModel.IsMarketsLoading = false;
        }

        public async Task LoadSelectedMarketRunners()
        {
            if (_mainViewModel.SelectedMarket == null)
                throw new NullReferenceException("_mainViewModel.SelectedMarketViewModel");

            if (_mainViewModel.SelectedMarket.Market == null)
                throw new NullReferenceException("_mainViewModel.SelectedMarketViewModel.Market");

            _mainViewModel.IsSelectedMarketRunnersLoading = true;

            _mainViewModel.SelectedMarket.RunnerList = null;
            _runnerViewModelDictionary.Clear();

            ICatalogRepository catalogRepository = _repositories.CatalogRepository;

            Market market = _mainViewModel.SelectedMarket.Market;

            List<Runner> runnerList = await market
                .GetRunnerListAsync(_token, catalogRepository);

            _mainViewModel.SelectedMarket.RunnerList = runnerList
                .Select(r => new RunnerViewModel { Runner = r, OfferedBetList = new ObservableCollection<Bet>() })
                .ToObservableCollection();

            _mainViewModel.IsSelectedMarketRunnersLoading = false;
        }

        public async Task LoadSelectedMarketPrices()
        {
            if (_mainViewModel.SelectedMarket == null)
                throw new NullReferenceException("_mainViewModel.SelectedMarket");

            if (_mainViewModel.SelectedMarket.Market == null)
                throw new NullReferenceException("_mainViewModel.SelectedMarket.Market");

            if (_mainViewModel.SelectedMarket.RunnerList == null)
                throw new NullReferenceException("_mainViewModel.SelectedMarket.RunnerList");

            IPricesRepository pricesRepository = _repositories.PricesRepository;

            Market market = _mainViewModel.SelectedMarket.Market;

            List<Price> priceList = await market
                .GetPriceListAsync(_token, pricesRepository);

            if (_mainViewModel.SelectedMarket != null && _mainViewModel.SelectedMarket.RunnerList != null)
            {
                _mainViewModel.SelectedMarket.IsMarketInPlay = market.IsMarketInPlay;

                foreach (Price price in priceList)
                {
                    foreach (RunnerViewModel runnerViewModel in _mainViewModel.SelectedMarket.RunnerList)
                    {
                        if (runnerViewModel.Runner.Id == price.RunnerId)
                        {
                            runnerViewModel.Price = price;
                            runnerViewModel.BestBackPrice = price.GetBestBackPrice();
                            runnerViewModel.BestLayPrice = price.GetBestLayPrice();

							if (runnerViewModel.OfferedBetList == null)
								throw new NullReferenceException("runnerViewModel.OfferedBetList");

                            runnerViewModel.BetCount = 0;
							runnerViewModel.OfferedBetList.Clear();
                            runnerViewModel.BackBet = null;
                            runnerViewModel.LayBet = null;
                        }
                    }
                }
            }
        }

        public void OfferBets()
        {
            if (_mainViewModel.SelectedMarket == null)
                throw new NullReferenceException("_mainViewModel.SelectedMarket");

            if (_mainViewModel.SelectedMarket.Market == null)
                throw new NullReferenceException("_mainViewModel.SelectedMarket.Market");

            if (_mainViewModel.SelectedMarket.RunnerList == null)
                throw new NullReferenceException("_mainViewModel.SelectedMarket.RunnerList");

            IBettingStrategy bettingStrategy = _strategies.CurrentBettingStrategy;

            Market market = _mainViewModel
                .SelectedMarket
                .Market;

            List<Price> prices = _mainViewModel
                .SelectedMarket
                .RunnerList
                .Select(r => r.Price)
                .ToList();

            List<PlacedBet> placedBets = _mainViewModel.PlacedBetList != null
                ? _mainViewModel.PlacedBetList.Select(cm => cm.PlacedBet).ToList()
                : new List<PlacedBet>();

            List<Bet> betList = bettingStrategy.OfferBets(market, prices, placedBets);

            foreach (Bet bet in betList)
            {
                foreach (RunnerViewModel runnerViewModel in _mainViewModel.SelectedMarket.RunnerList)
                {
                    if (runnerViewModel.Runner.Id == bet.RunnerId)
                    {
                        if (runnerViewModel.OfferedBetList == null)
                            runnerViewModel.OfferedBetList = new ObservableCollection<Bet>();

                        runnerViewModel.BetCount++;
                        runnerViewModel.OfferedBetList.Add(bet);
                        runnerViewModel.RaisePropertyChanged("OfferedBetList");

                        if (bet.Type == BetType.Back)
                            runnerViewModel.BackBet = bet;
                        else if (bet.Type == BetType.Lay)
                            runnerViewModel.LayBet = bet;
                    }
                }
            }
        }

        public async Task PlaceBetsAsync(ObservableCollection<Bet> bets)
        {
            if (bets == null || bets.Count == 0)
                return;

            IBetsRepository bettingRepository = _repositories.BetsRepository;

            Market market = _mainViewModel.SelectedMarket.Market;

            List<PlacedBet> placedBetList = await market.PlaceBetsAsync(_token, bettingRepository, bets);

            if (_mainViewModel.PlacedBetList == null)
                _mainViewModel.PlacedBetList = new ObservableCollection<PlacedBetViewModel>();

            _mainViewModel.PlacedBetList
                .AddRange(placedBetList.Select(pb => new PlacedBetViewModel { PlacedBet = pb, Market = market }));

            foreach (RunnerViewModel runnerViewModel in _mainViewModel.SelectedMarket.RunnerList)
            {
                runnerViewModel.BetCount = 0;
                runnerViewModel.OfferedBetList.Clear();
                runnerViewModel.LayBet = null;
                runnerViewModel.BackBet = null;
            }
        }

    }
}
