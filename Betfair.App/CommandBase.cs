﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace Betfair.App
{
    public abstract class CommandBase : DependencyObject, ICommand
    {
        public event EventHandler CanExecuteChanged;

        public void RaiseCanExecuteChanged()
        {
            EventHandler handler = CanExecuteChanged;
            if (handler != null)
            {
                EventArgs e = EventArgs.Empty;
                handler(this, e);
            }
        }

        public abstract bool CanExecute(object parameter);
        public abstract void Execute(object parameter);
    }

    public abstract class CommandBase<TParam> : CommandBase
    {
        [DebuggerStepThrough]
        public override bool CanExecute(object parameter)
        {
            return CanExecute(parameter != null ? (TParam)parameter : default(TParam));
        }

        protected abstract bool CanExecute(TParam parameter);

        [DebuggerStepThrough]
        public override void Execute(object parameter)
        {
            Execute(parameter != null ? (TParam)parameter : default(TParam));
        }

        protected abstract void Execute(TParam parameter);
    }

    public class EmptyCommandParameter { }
}
