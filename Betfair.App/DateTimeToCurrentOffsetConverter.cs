﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Betfair.App
{
	public class DateTimeToCurrentOffsetConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			DateTime source = (DateTime)value;

			TimeSpan offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now);

			DateTime result = source.AddMilliseconds(offset.TotalMilliseconds);

			return result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
