﻿using Betworm.Domain;
using System.Collections.ObjectModel;

namespace Betfair.App
{
    public class MarketViewModel : ViewModelBase
    {
        private Event _event;
        public Event Event
        {
            get { return _event; }
            set
            {
                _event = value;
                RaisePropertyChanged("Event");
            }
        }

        private Market _market;
        public Market Market 
        { 
            get { return _market; }
            set
            {
                _market = value;
                RaisePropertyChanged("Market");
            }
        }

        private ObservableCollection<RunnerViewModel> _runnerList;
        public ObservableCollection<RunnerViewModel> RunnerList
        {
            get { return _runnerList; }
            set
            {
                _runnerList = value;
                RaisePropertyChanged("RunnerList");
            }
        }

        private bool? _isMarketInPlay;
        public bool? IsMarketInPlay
        {
            get { return _isMarketInPlay; }
            set
            {
                _isMarketInPlay = value;
                RaisePropertyChanged("IsMarketInPlay");
            }
        }
    }
}
