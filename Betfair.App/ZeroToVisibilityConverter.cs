﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Betfair.App
{
    public class ZeroToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int intVal = System.Convert.ToInt32(value);

            Visibility visibility = intVal == 0 ? Visibility.Collapsed : Visibility.Visible;
            return visibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
