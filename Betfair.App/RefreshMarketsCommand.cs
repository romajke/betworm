﻿
namespace Betfair.App
{
    public class RefreshMarketsCommand : CommandBase
    {
        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override async void Execute(object parameter)
        {
            await Controller.Instance.LoadMarkets();
        }
    }
}
