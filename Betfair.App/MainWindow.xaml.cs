﻿using Betworm.Domain;
using Betworm.Strategies;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Betfair.App
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Loaded += OnMainWindowLoaded;

            Controller.Instance.MainViewModel.IsLoggedIn = false;
        }
       

        private async void OnMainWindowLoaded(object sender, RoutedEventArgs e)
        {
            DataContext = Controller.Instance.MainViewModel;

            await Controller.Instance.Login(keepAlive: true);

            await Controller.Instance.LoadMarkets();
        }
    }
}
