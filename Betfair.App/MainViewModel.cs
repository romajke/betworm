﻿using Betworm.Domain;
using System.Collections.ObjectModel;

namespace Betfair.App
{
    public class MainViewModel : ViewModelBase
    {
        private ObservableCollection<MarketViewModel> _marketList;
        public ObservableCollection<MarketViewModel> MarketList
        {
            get { return _marketList; }
            set
            {
                _marketList = value;
                RaisePropertyChanged("MarketList");
            }
        }

        private bool _isMarketsLoading;
        public bool IsMarketsLoading
        {
            get { return _isMarketsLoading; }
            set
            {
                _isMarketsLoading = value;
                RaisePropertyChanged("IsMarketsLoading");
            }
        }

        private MarketViewModel _selectedMarket;
        public MarketViewModel SelectedMarket
        {
            get { return _selectedMarket; }
            set
            {
                _selectedMarket = value;
                RaisePropertyChanged("SelectedMarket");
            }
        }

        private bool _isSelectedMarketRunnersLoading;
        public bool IsSelectedMarketRunnersLoading
        {
            get { return _isSelectedMarketRunnersLoading; }
            set
            {
                _isSelectedMarketRunnersLoading = value;
                RaisePropertyChanged("IsSelectedMarketRunnersLoading");
            }
        }

        private bool _isLoggedIn;
        public bool IsLoggedIn
        {
            get { return _isLoggedIn; }
            set
            {
                _isLoggedIn = value;
                RaisePropertyChanged("IsLoggedIn");
            }
        }

        private ObservableCollection<PlacedBetViewModel> _placedBetList;
        public ObservableCollection<PlacedBetViewModel> PlacedBetList
        {
            get { return _placedBetList; }
            set
            {
                _placedBetList = value;
                RaisePropertyChanged("PlacedBetList");
            }
        }
    }
}
