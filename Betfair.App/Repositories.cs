﻿using Betform.Data.Sample;
using Betworm.Data.Remote;
using Betworm.Domain;
using System;

namespace Betfair.App
{
    // TODO: may create a repository factory
    public class Repositories
    {
        private readonly ISessionRepository _sessionRepository;
        private readonly ICatalogRepository _catalogRepository;
        private readonly IPricesRepository _pricesRepository;
        private readonly IBetsRepository _betsRepository;

        public Repositories(string code)
        {
            switch (code)
            {
                case "Remote":
                    _sessionRepository = new RemoteSessionRepository();
                    _catalogRepository = new RemoteCatalogRepository();
                    _pricesRepository = new RemotePricesRepository();
                    _betsRepository = new RemoteBetsRepository();
                    break;
                case "Sample":
                    _sessionRepository = new SampleSessionRepository();
                    _catalogRepository = new SampleCatalogRepository();
                    _pricesRepository = new SamplePricesRepository();
                    _betsRepository = new SampleBetsRepository();
                    break;
                case "Readonly":
                    _sessionRepository = new RemoteSessionRepository();
                    _catalogRepository = new RemoteCatalogRepository();
                    _pricesRepository = new RemotePricesRepository();
                    _betsRepository = new SampleBetsRepository(); // sample
                    break;
                default:
                    throw new Exception("invalid_repository_code");
            }
        }

        public ISessionRepository SessionRepository { get { return _sessionRepository; } }
        public ICatalogRepository CatalogRepository { get { return _catalogRepository; } }
        public IPricesRepository PricesRepository { get { return _pricesRepository; } }
        public IBetsRepository BetsRepository { get { return _betsRepository; } }
    }
}
