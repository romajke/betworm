﻿using Betworm.Domain;
using Betworm.Strategies;
using System;
using System.Configuration;
using System.Globalization;

namespace Betfair.App
{
    public class Strategies
    {
        private IBettingStrategy _currentBettingStrategy;
        private string _currentStrategyCode;

		private readonly CultureInfo _cultureInfo = CultureInfo.InvariantCulture;

        private IBettingStrategy GetLeaderLayBackStrategy()
        {
            IBettingStrategy bettingStrategy = new LeaderLayBackStrategy(
                maxDistance: ConfigurationManager.AppSettings["strategy_leader_lay_back_max_distance"],
				minLayPrice: Convert.ToDecimal(ConfigurationManager.AppSettings["strategy_leader_lay_back_min_lay_price"], _cultureInfo),
				maxLayPrice: Convert.ToDecimal(ConfigurationManager.AppSettings["strategy_leader_lay_back_max_lay_price"], _cultureInfo),
				laySize: Convert.ToDecimal(ConfigurationManager.AppSettings["strategy_leader_lay_back_lay_size"], _cultureInfo),
				backSize: Convert.ToDecimal(ConfigurationManager.AppSettings["strategy_leader_lay_back_back_size"], _cultureInfo));

            return bettingStrategy;
        }


        public IBettingStrategy CurrentBettingStrategy
        {
            get 
            {
                string currentStrategyCode = ConfigurationManager.AppSettings["strategy_current_code"];

                if (_currentStrategyCode != currentStrategyCode)
                {
                    _currentStrategyCode = currentStrategyCode;

                    switch (_currentStrategyCode)
                    {
                        case "strategy_leader_lay_back":
                            _currentBettingStrategy = GetLeaderLayBackStrategy();
                            break;
                        default: throw new Exception("invalid strategy code");
                    }
                }

                return _currentBettingStrategy;
            }
        }
    }
}
