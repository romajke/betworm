﻿using Betworm.Domain;
using System.Collections.ObjectModel;

namespace Betfair.App
{
    public class MakeBetsCommand : CommandBase
    {
        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override async void Execute(object parameter)
        {
            RunnerViewModel runnerViewModel = (RunnerViewModel)parameter;
            if (runnerViewModel == null)
                return;

            await Controller.Instance.PlaceBetsAsync(runnerViewModel.OfferedBetList);
        }
    }
}
