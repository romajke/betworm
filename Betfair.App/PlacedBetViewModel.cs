﻿using Betworm.Domain;

namespace Betfair.App
{
    public class PlacedBetViewModel : ViewModelBase
    {
        private PlacedBet _placedBet;
        public PlacedBet PlacedBet
        {
            get { return _placedBet; }
            set
            {
                _placedBet = value;
                RaisePropertyChanged("PlacedBet");
            }
        }

        private Market _market;
        public Market Market
        {
            get { return _market; }
            set
            {
                _market = value;
                RaisePropertyChanged("Market");
            }
        }
    }
}
