﻿using Betworm.Domain;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Betform.Data.Sample
{
    public class SampleBetsRepository : IBetsRepository
    {
        public async Task<List<PlacedBet>> PlaceBetsAsync(string token, List<Bet> bets)
        {
            return await Task.Factory.StartNew<List<PlacedBet>>(() =>
            {
                Thread.Sleep(100);

                List<PlacedBet> betPlacementResultList = new List<PlacedBet>();

                int i = 0;

                foreach (Bet bet in bets)
                {
                    i++;

                    PlacedBet betPlacementResult = new PlacedBet(
                        betId: bet.MarketId * 10 + i,
                        averagePriceMatched: bet.Price,
                        sizeMatched: bet.Size,
                        success: true,
                        resultCode: "OK");

                    betPlacementResultList.Add(betPlacementResult);
                }

                return betPlacementResultList;
            });
        }
    }
}
