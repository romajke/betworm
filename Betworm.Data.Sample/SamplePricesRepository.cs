﻿using Betworm.Domain;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Betform.Data.Sample
{
    public class SamplePricesRepository : IPricesRepository
    {
        public async Task<Market.PricesResult> GetMarketPriceListAsync(string token, int marketId)
        {
            return await Task.Factory.StartNew<Market.PricesResult>(() =>
            {
                Thread.Sleep(500);

                Dictionary<int, decimal> diffDictionary = new Dictionary<int, decimal>();
                Random random = new Random(DateTime.UtcNow.Millisecond);

                Func<decimal, int, decimal> r = (value, id) =>
                {
                    decimal diff;
                    if (!diffDictionary.TryGetValue(id, out diff))
                    {
                        diff = Convert.ToDecimal(random.NextDouble()) * (value * 0.1m) - 0.5m;
                        diffDictionary.Add(id, diff);
                    }

                    decimal randomized = value + diff;
                    return randomized;
                };

                List<Price> priceList = null;

                switch (marketId)
                {
                    case 11:
                        priceList = new List<Price>
                        {
                            new Price(runnerId: 1, backPriceList: new[] { r(1.8m, 1), r(1.82m, 1), r(1.84m, 1) }, layPriceList: new[] { r(1.86m, 1), r(1.88m, 1), r(1.9m, 1) }),
                            new Price(runnerId: 2, backPriceList: new[] { r(2.35m, 2), r(2.4m, 2), r(2.45m, 2) }, layPriceList: new[] { r(2.6m, 2), r(2.65m, 2), r(2.7m, 2) }),
                            new Price(runnerId: 3, backPriceList: new[] { r(3.8m, 3), r(3.9m, 3), r(4m, 3) }, layPriceList: new[] { r(4.1m, 3), r(4.2m, 3), r(4.3m, 3) }),
                            new Price(runnerId: 4, backPriceList: new[] { r(8.5m, 4), r(9m, 4), r(10m, 4) }, layPriceList: new[] { r(11m, 4), r(13m, 4), r(13.5m, 4) }),
                            new Price(runnerId: 5, backPriceList: new[] { r(16m, 5), r(18m, 5), r(19m, 5) }, layPriceList: new[] { r(20m, 5), r(21m, 5), r(22m, 5) })
                        };
                        break;
                    case 12:
                        priceList = new List<Price>
                        {
                            new Price(runnerId: 1, backPriceList: new[] { r(1.2m, 1), r(1.21m, 1), r(1.22m, 1) }, layPriceList: new[] { r(1.23m, 1), r(1.24m, 1), r(1.25m, 1) }),
                            new Price(runnerId: 2, backPriceList: new[] { r(1.44m, 2), r(1.46m, 2), r(1.48m, 2) }, layPriceList: new[] { r(1.5m, 2), r(1.52m, 2), r(1.54m, 2) }),
                            new Price(runnerId: 3, backPriceList: new[] { r(2.06m, 3), r(2.1m, 3), r(2.12m, 3) }, layPriceList: new[] { r(2.14m, 3), r(2.16m, 3), r(2.18m, 3) }),
                            new Price(runnerId: 4, backPriceList: new[] { r(2.3m, 4), r(2.36m, 4), r(2.4m, 4) }, layPriceList: new[] { r(2.42m, 4), r(2.44m, 4), r(2.5m, 4) }),
                            new Price(runnerId: 5, backPriceList: new[] { r(2.9m, 5), r(2.96m, 5), r(2.98m, 5) }, layPriceList: new[] { r(3m, 5), r(3.08m, 5), r(3.14m, 5) })
                        };
                        break;
                }

                return new Market.PricesResult
                {
                    IsMarketInPlay = false,
                    PriceList = priceList ?? new List<Price>()
                };
            });
        }
    }
}
