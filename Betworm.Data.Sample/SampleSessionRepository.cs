﻿using Betworm.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Betform.Data.Sample
{
    public class SampleSessionRepository : ISessionRepository
    {

        public async Task<string> GetSessionTokenAsync(string userName, string password)
        {
            return await Task.Factory.StartNew<string>(() =>
            {
                Thread.Sleep(600);

                string token = Guid.NewGuid().ToString();
                return token;
            });
        }

        public async Task<string> ProlongSessionAsync(string token)
        {
            return await Task.Factory.StartNew<string>(() =>
            {
                Thread.Sleep(600);

                token = Guid.NewGuid().ToString();
                return token;
            });
        }
    }
}
