﻿using Betworm.Domain;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Betform.Data.Sample
{
    public class SampleCatalogRepository : ICatalogRepository
    {
        public async Task<List<Event>> GetEventListAsync(string token, int parentEventId)
        {
            return await Task.Factory.StartNew<List<Event>>(() =>
            {
                Thread.Sleep(100);

                List<Event> eventList = new List<Event>
                {
                    new Event(1, "Ellada Racing Today")
                };

                return eventList;
            });
        }

        public async Task<List<Market>> GetMarketListAsync(string token, int eventId)
        {
            return await Task.Factory.StartNew<List<Market>>(() =>
            {
                Thread.Sleep(1200);

                List<Market> marketList = new List<Market>
                {
                    new Market(11, DateTime.UtcNow.AddSeconds(20), "Ath 1m Mdn Stks", 1, "Athens"),
                    new Market(12, DateTime.UtcNow.AddSeconds(20), "Ath To Be Placed", 3, "Athens")
                };

                return marketList;
            });
        }

        public async Task<List<Runner>> GetRunnerListAsync(string token, int marketId)
        {
            return await Task.Factory.StartNew<List<Runner>>(() =>
            {
                Thread.Sleep(600);

                List<Runner> runnerList = new List<Runner>
                {
                    new Runner(1, "Socrates"),
                    new Runner(2, "Platon"),
                    new Runner(3, "Aristotel"),
                    new Runner(4, "Diogen"),
                    new Runner(5, "Demokrit")
                };

                return runnerList;
            });
        }
    }
}
